package applovin_cm

import "github.com/go-playground/validator/v10"

var validate = validator.New()

func init() {
	validate.SetTagName("binding")
}
