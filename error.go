package applovin_cm

import (
	"fmt"
	"io"
	"net/http"
	"strings"
)

type StatusError struct {
	path       string
	statusCode int
	status     string
	headers    http.Header
	body       []byte
	readErr    error
}

func newStatusError(path string, statusCode int) *StatusError {
	return &StatusError{
		path:       path,
		statusCode: statusCode,
		status:     fmt.Sprintf("%d %s", statusCode, http.StatusText(statusCode)),
	}
}

func (s *StatusError) StatusCode() int {
	return s.statusCode
}

func (s *StatusError) Headers() http.Header {
	return s.headers
}

func (s *StatusError) Body() ([]byte, error) {
	return s.body, s.readErr
}

const includeHeaderToErr = false

func (s *StatusError) Error() string {
	var builder strings.Builder
	switch {
	case s.readErr != nil:
		builder.WriteString(fmt.Sprintf("applovin_cm: %s: %s: <can't read err message: %s>", s.path, s.status, s.readErr))
	case len(s.body) > 0:
		builder.WriteString(fmt.Sprintf("applovin_cm: %s: %s: %s", s.path, s.status, string(s.body)))
	default:
		builder.WriteString(fmt.Sprintf("applovin_cm: %s: %s", s.path, s.status))
	}
	if includeHeaderToErr && len(s.headers) > 0 {
		builder.WriteString("\nHeaders:\n")
		_ = s.headers.Write(&builder)
	}
	return builder.String()
}

func statusErr(resp *http.Response) *StatusError {
	body, err := io.ReadAll(resp.Body)
	_ = resp.Body.Close()
	return &StatusError{
		path:       resp.Request.URL.Path,
		status:     resp.Status,
		statusCode: resp.StatusCode,
		headers:    resp.Header.Clone(),
		body:       body,
		readErr:    err,
	}
}

func wrapErr(err error, where ...string) error {
	w := ""
	if len(where) > 0 {
		w = where[0]
	}
	if w == "" {
		return fmt.Errorf("applovin_cm: %w", err)
	} else {
		return fmt.Errorf("applovin_cm: %s: %w", w, err)
	}
}
