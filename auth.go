package applovin_cm

import (
	"net/http"

	"github.com/TelpeNight/oauthctx"
	"golang.org/x/oauth2"
)

var Endpoint = oauth2.Endpoint{
	AuthURL:   "https://oauth.applovin.com/oauth/initialize",
	TokenURL:  "https://oauth.applovin.com/oauth/v1/access_token",
	AuthStyle: oauth2.AuthStyleInParams,
}

type Auth interface {
	Client() *http.Client
}

func NewAuth(ops ...AuthOption) Auth {
	var config authOps
	for _, op := range ops {
		op(&config)
	}
	var transport http.RoundTripper
	// from innermost to topmost
	if config.tokenSrcCtx != nil {
		transport = &oauthctx.Transport{
			Source: oauthctx.ReuseTokenSource(nil, config.tokenSrcCtx),
			Base:   transport,
		}
	}
	// wait before setting token to header, as it may expire during wait
	if config.waiter != nil {
		transport = &waitingTransport{config.waiter, transport}
	}
	return &auth{client: &http.Client{Transport: transport}}
}

type auth struct {
	client *http.Client
}

func (a *auth) Client() *http.Client {
	return a.client
}

type authOps struct {
	waiter      Waiter
	tokenSrcCtx oauthctx.TokenSource
}

type AuthOption func(*authOps)

func WithTokenSource(src oauthctx.TokenSource) AuthOption {
	return func(ops *authOps) {
		ops.tokenSrcCtx = src
	}
}

func WithRateLimit(waiter Waiter) AuthOption {
	return func(ops *authOps) {
		ops.waiter = waiter
	}
}
