package applovin_cm

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"golang.org/x/oauth2"
)

func WithOAuth2UnwrapTokenClient(ctx context.Context) context.Context {
	base, _ := ctx.Value(oauth2.HTTPClient).(*http.Client)
	return context.WithValue(ctx, oauth2.HTTPClient, unwrapTokenClient(base))
}

func unwrapTokenClient(client *http.Client) *http.Client {
	var base http.RoundTripper
	if client != nil {
		base = client.Transport
	}
	return &http.Client{Transport: &tokenTransport{base}}
}

type tokenTransport struct {
	base http.RoundTripper
}

func (t *tokenTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	resp, err := t.roundTrip(req)
	if err != nil {
		return nil, err
	}

	body, err := io.ReadAll(io.LimitReader(resp.Body, 1<<20))
	_ = resp.Body.Close()
	if err != nil {
		return nil, fmt.Errorf("applovin_cm.tokenTransport: can't get token: %w", err)
	}

	var response struct {
		AccessToken json.RawMessage `json:"accessToken"`
	}
	unmarshalErr := json.Unmarshal(body, &response)
	if unmarshalErr == nil && len(response.AccessToken) > 0 {
		body = response.AccessToken
	}
	resp2 := *resp
	resp2.Body = io.NopCloser(bytes.NewReader(body))

	return &resp2, nil
}

func (t *tokenTransport) roundTrip(req *http.Request) (*http.Response, error) {
	if t.base == nil {
		return http.DefaultTransport.RoundTrip(req)
	}
	return t.base.RoundTrip(req)
}
