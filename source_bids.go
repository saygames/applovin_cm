package applovin_cm

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
)

type SourceBids struct {
	sourceBidsMap map[sourceBidKey][]SourceBid
}

type sourceBidKey struct {
	appIdExt string
	country  string
}

type SourceBid struct {
	AppId                 string `json:"app_id,omitempty" binding:"required"`
	AppIdExternal         string `json:"app_id_external,omitempty"` //Необязательно для заполенения при обновлении source bid
	BidOverridePercentage string `json:"bid_override_percentage,omitempty" binding:"required"`
	Bid                   string `json:"bid_value,omitempty" binding:"required"`
	Country               string `json:"country,omitempty" binding:"required"`
}

// sourceBidUpdate Используется для формирования массива для отправки в апи
type sourceBidUpdate struct {
	SourceBids []SourceBid `json:"source_bids,omitempty" binding:"required"`
}

func (s *SourceBid) IsDefaultBid() bool {
	return s != nil && strings.ToLower(s.Bid) == defaultBidValue
}

func (s *SourceBid) formatToUpload() {
	if s != nil {
		s.Country = strings.ToLower(s.Country)
		s.Bid = strings.ToLower(s.Bid)
		s.BidOverridePercentage = strings.ToLower(s.BidOverridePercentage)
	}
}

const (
	defaultBidValue = "default"
)

// GetSourceBids https://dash.applovin.com/documentation/mediation/auctions/campaign-management-api#/source_bids/campaign_id
func (c *client) GetSourceBids(ctx context.Context, campaignId string) (SourceBids, error) {
	resp, err := c.get(ctx, "/source_bids/"+campaignId)
	if err != nil {
		return SourceBids{}, err
	}
	defer resp.Body.Close()

	return parseSourceBidsFromResponse(resp)
}

// UpdateSourceBids https://dash.applovin.com/documentation/mediation/auctions/campaign-management-api#/source_bids/campaign_id
func (c *client) UpdateSourceBids(ctx context.Context, campaignId string, sourceBids []SourceBid) (SourceBids, error) {
	resp, err := c.post(ctx, "/source_bids/"+campaignId, createSourceBidUpdate(sourceBids))
	if err != nil {
		return SourceBids{}, err
	}
	defer resp.Body.Close()

	return parseSourceBidsFromResponse(resp)
}

func (sourceBids *SourceBids) GetByAppIdExtAndCountry(appIdExt string, country string) []SourceBid {
	var sourceKey = sourceBidKey{
		appIdExt: appIdExt,
		country:  getFormattedCountry(country),
	}

	return sourceBids.sourceBidsMap[sourceKey]
}

func parseSourceBidsFromResponse(resp *http.Response) (SourceBids, error) {

	var sourceBids []SourceBid
	err := json.NewDecoder(resp.Body).Decode(&sourceBids)
	if err != nil {
		return SourceBids{}, wrapErr(err, "Client.parseSourceBidsFromResponse: can't parse json")
	}

	var sourceBidsMap = make(map[sourceBidKey][]SourceBid, len(sourceBids))

	for _, sourceBidsResponse := range sourceBids {

		var appIdExt = sourceBidsResponse.AppIdExternal
		var country = getFormattedCountry(sourceBidsResponse.Country)
		var bid = sourceBidsResponse.Bid

		var sourceKey = sourceBidKey{
			appIdExt: appIdExt,
			country:  country,
		}

		source := SourceBid{
			AppId:                 sourceBidsResponse.AppId,
			AppIdExternal:         appIdExt,
			BidOverridePercentage: sourceBidsResponse.BidOverridePercentage,
			Bid:                   bid,
			Country:               country,
		}

		sourceBidsMap[sourceKey] = append(sourceBidsMap[sourceKey], source)
	}

	return SourceBids{
		sourceBidsMap: sourceBidsMap,
	}, nil
}

func createSourceBidUpdate(sourceBids []SourceBid) sourceBidUpdate {
	for i := range sourceBids {
		sourceBids[i].formatToUpload()
	}

	return sourceBidUpdate{
		SourceBids: sourceBids,
	}
}

func getFormattedCountry(country string) string {
	return strings.ToUpper(country)
}
