package applovin_cm

import "testing"

func TestStatus_UnmarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		s       Status
		bytes   string
		wantErr bool
	}{
		{
			name:    "true",
			s:       StatusTrue,
			bytes:   "true",
			wantErr: false,
		},
		{
			name:    "false",
			s:       StatusFalse,
			bytes:   "false",
			wantErr: false,
		},
		{
			name:    "null",
			s:       StatusNull,
			bytes:   "null",
			wantErr: false,
		},
		{
			name:    "1",
			s:       StatusNull,
			bytes:   "1",
			wantErr: true,
		},
		{
			name:    `"true"`,
			s:       StatusNull,
			bytes:   `"true"`,
			wantErr: true,
		},
		{
			name:    `"false"`,
			s:       StatusNull,
			bytes:   `"false"`,
			wantErr: true,
		},
		{
			name:    `"null"`,
			s:       StatusNull,
			bytes:   `"null"`,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var s Status
			if err := s.UnmarshalJSON([]byte(tt.bytes)); (err != nil) != tt.wantErr || s != tt.s {
				t.Errorf("UnmarshalJSON() status = %v, wantStatus = %v, error = %v, wantErr %v", s, tt.s, err, tt.wantErr)
			}
		})
	}
}
