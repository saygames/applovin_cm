package applovin_cm

import (
	"context"
	"fmt"

	"golang.org/x/sync/errgroup"
)

type QueryCampaign struct {
	Campaign        Campaign
	CampaignTargets *CampaignTargets
	SourceBids      *SourceBids
	Sources         *Sources
}

type QueryCampaignFields uint

const (
	QueryCampaignTargets QueryCampaignFields = 1 << iota
	QueryCampaignSourceBids
	QueryCampaignSources
)

type QueryCampaignFilter = func(Campaign) bool

func AllCampaigns(_ Campaign) bool {
	return true
}

func (c *client) QueryCampaigns(ctx context.Context, fields QueryCampaignFields, filter QueryCampaignFilter) ([]QueryCampaign, error) {
	return queryCampaigns(ctx, c, fields, filter)
}

func queryCampaigns(ctx context.Context, client Client, fields QueryCampaignFields, filter QueryCampaignFilter) ([]QueryCampaign, error) {
	campaigns, err := client.ListCampaigns(ctx)
	if err != nil {
		return nil, err
	}

	result := make([]QueryCampaign, 0, 10)
	for _, c := range campaigns {
		if !filter(c) {
			continue
		}
		result = append(result, QueryCampaign{Campaign: c})
	}

	if fields == 0 || len(result) == 0 {
		return result, nil
	}

	group, ctx := errgroup.WithContext(ctx)
	for i := range result {
		i := i
		group.Go(func() (err error) {
			row := result[i]
			campaignId := row.Campaign.CampaignId

			defer func() {
				if err != nil {
					err = fmt.Errorf("%w (%d request)", err, i)
				}
			}()

			if (fields & QueryCampaignTargets) != 0 {
				targets, err := client.GetCampaignTargets(ctx, campaignId)
				if err != nil {
					return err
				}
				row.CampaignTargets = &targets
			}

			if (fields & QueryCampaignSourceBids) != 0 {
				sourceBids, err := client.GetSourceBids(ctx, campaignId)
				if err != nil {
					return err
				}
				row.SourceBids = &sourceBids
			}

			if (fields & QueryCampaignSources) != 0 {
				sources, err := client.GetSources(ctx, campaignId)
				if err != nil {
					return err
				}
				row.Sources = &sources
			}

			result[i] = row
			return nil
		})
	}

	err = group.Wait()
	if err != nil {
		return nil, err
	}
	return result, nil
}
