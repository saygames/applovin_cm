module bitbucket.org/saygames/applovin_cm

go 1.22

require (
	github.com/TelpeNight/oauthctx v0.3.1
	github.com/go-playground/validator/v10 v10.10.1
	golang.org/x/oauth2 v0.24.0
	golang.org/x/sync v0.10.0
)

require (
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	golang.org/x/crypto v0.31.0 // indirect
	golang.org/x/sys v0.28.0 // indirect
	golang.org/x/text v0.21.0 // indirect
)
