package applovin_cm

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
)

const (
	host     = "https://o.applovin.com"
	section  = "/campaign_management"
	version  = "v1"
	basePath = section + "/" + version
	baseUrl  = host + basePath
)

type Client interface {
	ListCampaigns(ctx context.Context) ([]Campaign, error)
	GetCampaign(ctx context.Context, campaignId string) (Campaign, error)
	UpdateCampaign(ctx context.Context, campaign Campaign) (Campaign, error)
	QueryCampaigns(ctx context.Context, fields QueryCampaignFields, filter QueryCampaignFilter) ([]QueryCampaign, error)
	GetCampaignTargets(ctx context.Context, campaignId string) (CampaignTargets, error)
	UpdateCampaignTargets(ctx context.Context, targets CampaignTargetsUpdate) (CampaignTargets, error)
	GetSourceBids(ctx context.Context, campaignId string) (SourceBids, error)
	UpdateSourceBids(ctx context.Context, campaignId string, sourceBids []SourceBid) (SourceBids, error)
	GetSources(ctx context.Context, campaignId string) (Sources, error)
}

func NewClient(auth Auth) Client {
	return &client{auth}
}

type client struct {
	auth Auth
}

func (c *client) get(ctx context.Context, path string) (*http.Response, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, baseUrl+path, nil)
	if err != nil {
		return nil, wrapErr(err, "client.get")
	}
	resp, err := c.auth.Client().Do(req)
	if err != nil {
		return nil, wrapErr(err, "client.get")
	}
	if resp.StatusCode != http.StatusOK {
		return nil, statusErr(resp)
	}
	return resp, err
}

func (c *client) post(ctx context.Context, path string, i interface{}) (*http.Response, error) {
	if err := validate.StructCtx(ctx, i); err != nil {
		return nil, err
	}
	body, err := json.Marshal(i)
	if err != nil {
		return nil, wrapErr(err, "client.post: can't marshal json")
	}
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, baseUrl+path, bytes.NewReader(body))
	if err != nil {
		return nil, wrapErr(err, "client.post")
	}
	resp, err := c.auth.Client().Do(req)
	if err != nil {
		return nil, wrapErr(err, "client.post")
	}
	if resp.StatusCode != http.StatusOK {
		return nil, statusErr(resp)
	}
	return resp, nil
}
