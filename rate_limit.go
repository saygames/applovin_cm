package applovin_cm

import (
	"context"
	"net/http"
)

type Waiter interface {
	Wait(ctx context.Context) error
}

type waitingTransport struct {
	waiter    Waiter
	transport http.RoundTripper
}

func (w *waitingTransport) RoundTrip(request *http.Request) (*http.Response, error) {
	err := w.waiter.Wait(request.Context())
	if err != nil {
		return nil, err
	}
	return w.base().RoundTrip(request)
}

func (w *waitingTransport) base() http.RoundTripper {
	if w.transport == nil {
		return http.DefaultTransport
	}
	return w.transport
}
