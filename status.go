package applovin_cm

import (
	"encoding/json"
	"fmt"
)

type Status int

const (
	StatusFalse Status = -1
	StatusNull  Status = 0
	StatusTrue  Status = 1
)

//goland:noinspection GoMixedReceiverTypes
func (s Status) MarshalJSON() ([]byte, error) {
	switch s {
	case StatusFalse:
		return []byte("false"), nil
	case StatusNull:
		return []byte("null"), nil
	case StatusTrue:
		return []byte("true"), nil
	default:
		return nil, fmt.Errorf("unknown applovin_cm.Status %d", int(s))
	}
}

//goland:noinspection GoMixedReceiverTypes
func (s *Status) UnmarshalJSON(bytes []byte) error {
	*s = StatusNull
	if string(bytes) == "null" {
		return nil
	}
	var val bool
	err := json.Unmarshal(bytes, &val)
	if err != nil {
		return err
	}
	if val {
		*s = StatusTrue
	} else {
		*s = StatusFalse
	}
	return nil
}
