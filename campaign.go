package applovin_cm

import (
	"context"
	"encoding/json"
	"strconv"
	"strings"
	"time"
)

type Campaign struct {
	Name           string    `json:"name,omitempty"`
	CampaignId     string    `json:"campaign_id,omitempty"`
	PackageName    string    `json:"package_name,omitempty"`
	BundleId       string    `json:"bundle_id,omitempty"`
	Platform       string    `json:"platform,omitempty"`
	Category       string    `json:"category,omitempty"`
	BidType        string    `json:"bid_type,omitempty"`
	TrackingMethod string    `json:"tracking_method,omitempty"`
	Status         Status    `json:"status,omitempty"`
	CreatedAt      time.Time `json:"-"`
	Impressions    int       `json:"impressions,omitempty"`
	Clicks         int       `json:"clicks,omitempty"`
	Installs       int       `json:"installs,omitempty"`
	Spend          float64   `json:"-"`
	GoalPeriod     *int      `json:"goal_period,omitempty"`
}

const (
	BidTypeCPI           = "CPI"
	BidTypeCPP_GOAL      = "CPP_GOAL"
	BidTypeRET           = "RET"
	BidTypeCPE           = "CPE"
	BidTypeROAS_AD_BASED = "ROAS_AD_BASED"
	BidTypeROAS_IAP      = "ROAS_IAP"

	//Нет в документации
	BidTypeROAS_BLENDED = "BLD_ROAS"
	BidTypedCPM         = "dCPM"
)

func (c *Campaign) IsActive() bool {
	return c != nil && c.Status == StatusTrue
}

func (c *Campaign) HasGoalPeriod(targetGoalPeriod int) bool {
	return c != nil && c.GoalPeriod != nil && *c.GoalPeriod == targetGoalPeriod
}

// ListCampaigns https://dash.applovin.com/documentation/mediation/auctions/campaign-management-api#/campaigns
func (c *client) ListCampaigns(ctx context.Context) ([]Campaign, error) {
	resp, err := c.get(ctx, "/campaigns")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var result []Campaign
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return nil, wrapErr(err, "Client.ListCampaigns: can't parse json")
	}
	return result, nil
}

func (c *client) GetCampaign(ctx context.Context, campaignId string) (Campaign, error) {
	resp, err := c.get(ctx, "/campaign/"+campaignId)
	if err != nil {
		return Campaign{}, err
	}
	defer resp.Body.Close()
	var result Campaign
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return Campaign{}, wrapErr(err, "Client.GetCampaign: can't parse json")
	}
	return result, nil
}

func (c *client) UpdateCampaign(ctx context.Context, campaign Campaign) (Campaign, error) {
	path := "/campaign"
	if campaign.CampaignId != "" {
		path += "/" + campaign.CampaignId
	}
	resp, err := c.post(ctx, path, campaign)
	if err != nil {
		return Campaign{}, err
	}
	defer resp.Body.Close()
	var result Campaign
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return Campaign{}, wrapErr(err, "Client.UpdateCampaign: can't parse json")
	}
	return result, err
}

func (c *Campaign) UnmarshalJSON(bytes []byte) error {
	type CampaignJson Campaign
	var aux struct {
		CampaignJson
		CreatedAtStr string `json:"created_at"`
		SpendStr     string `json:"spend"`
	}
	err := json.Unmarshal(bytes, &aux)
	if err != nil {
		return err
	}
	*c = Campaign(aux.CampaignJson)
	c.CreatedAt, err = time.Parse("2006-01-02 15:04:05", aux.CreatedAtStr)
	if err != nil {
		return err
	}
	c.Spend, err = strconv.ParseFloat(strings.ReplaceAll(aux.SpendStr, ",", ""), 64)
	if err != nil {
		return err
	}
	return nil
}

// MarshalJSON marshals campaign for update
func (c Campaign) MarshalJSON() ([]byte, error) {
	c.Impressions = 0
	c.Clicks = 0
	c.Installs = 0
	type CampaignJson Campaign
	aux := CampaignJson(c)
	return json.Marshal(aux)
}
