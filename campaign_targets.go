package applovin_cm

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

type CampaignTargets struct {
	CampaignId        string
	BidType           string
	Categories        []string
	Countries         []string
	DeviceTypes       []string
	OsMajorVersionMin string
	countriesMap      map[string]CampaignTargetCountry
}

type CampaignTargetsUpdate struct {
	CampaignId        string                  `json:"-" binding:"required"`
	BidType           string                  `json:"bid_type,omitempty"`
	Categories        *[]string               `json:"categories,omitempty"`
	Countries         []CampaignTargetCountry `json:"countries"`
	DeviceTypes       []string                `json:"device_types,omitempty"`
	OsMajorVersionMin string                  `json:"os_major_version_min" binding:"numeric|eq=all"`
}

type CampaignTargetCountry struct {
	Country       string   `json:"-"`
	Bid           *float64 `json:"bid,omitempty"` //On CPI campaigns, this is the CPI in USD. For RET, ROAS_AD_BASED, and ROAS_IAP, this is the percent goal (50=50%). For CPP and CPE, this is the goal event value in USD
	Budget        *int     `json:"budget,omitempty"`
	ClickUrl      string   `json:"click_url,omitempty"`
	ImpressionUrl string   `json:"impression_url,omitempty"`
	Disabled      bool     `json:"disabled"`
}

func (t CampaignTargets) Default() CampaignTargetCountry {
	return t.countriesMap["DEFAULT"]
}

func (t CampaignTargets) Country(c string) (CampaignTargetCountry, bool) {
	target, ok := t.countriesMap[strings.ToUpper(c)]
	return target, ok
}

func (t CampaignTargets) Update() CampaignTargetsUpdate {
	countries := make([]CampaignTargetCountry, 0, len(t.countriesMap))
	for _, country := range t.countriesMap {
		countries = append(countries, country)
	}
	return CampaignTargetsUpdate{
		CampaignId:        t.CampaignId,
		BidType:           t.BidType,
		Categories:        updateCategories(t.Categories),
		Countries:         countries,
		DeviceTypes:       t.DeviceTypes,
		OsMajorVersionMin: t.OsMajorVersionMin,
	}
}

func (t CampaignTargets) UpdateCountries(countries []CampaignTargetCountry) CampaignTargetsUpdate {
	return CampaignTargetsUpdate{
		CampaignId:        t.CampaignId,
		BidType:           t.BidType,
		Categories:        updateCategories(t.Categories),
		Countries:         countries,
		DeviceTypes:       t.DeviceTypes,
		OsMajorVersionMin: t.OsMajorVersionMin,
	}
}

func updateCategories(c []string) *[]string {
	if c == nil {
		return nil
	}
	cp := make([]string, len(c))
	copy(cp, c)
	return &cp
}

func (u CampaignTargetsUpdate) Targets() (CampaignTargets, error) {
	result := CampaignTargets{
		CampaignId:        u.CampaignId,
		BidType:           u.BidType,
		Categories:        targetCategories(u.Categories),
		Countries:         make([]string, 0, len(u.Countries)),
		DeviceTypes:       u.DeviceTypes,
		OsMajorVersionMin: u.OsMajorVersionMin,
		countriesMap:      make(map[string]CampaignTargetCountry, len(u.Countries)),
	}
	for _, target := range u.Countries {
		country := strings.ToUpper(target.Country)
		if _, exists := result.countriesMap[country]; exists {
			return CampaignTargets{}, fmt.Errorf("applovin_cm: duplicate country %s in campaign %s targets", country, u.CampaignId)
		}
		result.countriesMap[country] = target
		if country != "DEFAULT" {
			result.Countries = append(result.Countries, country)
		}
	}
	return result, nil
}

func targetCategories(c *[]string) []string {
	if c == nil || *c == nil {
		return nil
	}
	cp := make([]string, len(*c))
	copy(cp, *c)
	return cp
}

// GetCampaignTargets https://dash.applovin.com/documentation/mediation/auctions/campaign-management-api#/campaign_targets/campaign_id
func (c *client) GetCampaignTargets(ctx context.Context, campaignId string) (CampaignTargets, error) {
	resp, err := c.get(ctx, "/campaign_targets/"+campaignId)
	if err != nil {
		return CampaignTargets{}, err
	}
	defer resp.Body.Close()
	return campaignTargetsResponse(campaignId, resp)
}

// UpdateCampaignTargets https://dash.applovin.com/documentation/mediation/auctions/campaign-management-api#/campaign_targets/campaign_id
func (c *client) UpdateCampaignTargets(ctx context.Context, targets CampaignTargetsUpdate) (CampaignTargets, error) {
	targets.BidType = ""
	resp, err := c.post(ctx, "/campaign_targets/"+targets.CampaignId, targets)
	if err != nil {
		return CampaignTargets{}, err
	}
	defer resp.Body.Close()
	return campaignTargetsResponse(targets.CampaignId, resp)
}

func campaignTargetsResponse(campaignId string, resp *http.Response) (CampaignTargets, error) {
	var response CampaignTargetsUpdate
	err := json.NewDecoder(resp.Body).Decode(&response)

	if err != nil {
		//var jsonErr *json.UnmarshalTypeError
		//if errors.As(err, &jsonErr) && jsonErr.Value == "array" && jsonErr.Offset == 1 {
		//	return CampaignTargets{}, newStatusError("/campaign_targets/"+campaignId, http.StatusNotFound)
		//}
		return CampaignTargets{}, wrapErr(err, "Client.GetCampaignTargets: can't parse json")
	}
	response.CampaignId = campaignId
	return response.Targets()
}

type campaignTargetCountryJson CampaignTargetCountry

func (t CampaignTargetCountry) MarshalJSON() ([]byte, error) {
	country := t.Country
	if country != "DEFAULT" {
		country = strings.ToLower(country)
	}
	return json.Marshal(map[string]campaignTargetCountryJson{
		country: campaignTargetCountryJson(t),
	})
}

func (t *CampaignTargetCountry) UnmarshalJSON(bytes []byte) error {
	var value map[string]campaignTargetCountryJson
	err := json.Unmarshal(bytes, &value)
	if err != nil {
		return err
	}
	if len(value) != 1 {
		return fmt.Errorf("applovin_cm.CampaignTargetCountry.UnmarshalJSON: unexpected map len %d", len(value))
	}
	for country, target := range value {
		*t = CampaignTargetCountry(target)
		t.Country = strings.ToUpper(country)
	}
	return nil
}
