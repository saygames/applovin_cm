package applovin_cm

import (
	"context"
	"encoding/json"
	"net/http"
)

type Source struct {
	AppId         string `json:"app_id,omitempty" binding:"required"`
	AppIdExternal string `json:"app_id_external,omitempty"` //Необязательно для заполенения при обновлении sources
	Status        Status `json:"status,omitempty" binding:"required"`
}

func (s *Source) IsActive() bool {
	return s != nil && s.Status == StatusTrue
}

type Sources struct {
	sourcesMap map[string][]Source
}

func (s *Sources) List() []Source {
	if s == nil {
		return nil
	}
	var result = make([]Source, 0, int(float64(len(s.sourcesMap))*1.5))
	for _, sources := range s.sourcesMap {
		result = append(result, sources...)
	}
	return result
}

func (s *Sources) GetByAppIdExt(appIdExt string) []Source {
	if s != nil {
		return s.sourcesMap[appIdExt]
	}

	return nil
}

// GetSources https://dash.applovin.com/documentation/mediation/auctions/campaign-management-api#/sources/campaign_id
func (c *client) GetSources(ctx context.Context, campaignId string) (Sources, error) {
	resp, err := c.get(ctx, "/sources/"+campaignId)
	if err != nil {
		return Sources{}, err
	}
	defer resp.Body.Close()

	return parseSourcesFromResponse(resp)
}

func parseSourcesFromResponse(resp *http.Response) (Sources, error) {
	var sources []Source
	err := json.NewDecoder(resp.Body).Decode(&sources)
	if err != nil {
		return Sources{}, wrapErr(err, "Client.parseSourcesFromResponse: can't parse json")
	}

	sourcesMap := map[string][]Source{}

	for _, source := range sources {
		appIdExt := source.AppIdExternal

		sourcesMap[appIdExt] = append(sourcesMap[appIdExt], source)
	}

	return Sources{
		sourcesMap: sourcesMap,
	}, err
}
